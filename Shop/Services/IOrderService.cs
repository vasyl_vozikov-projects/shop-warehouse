﻿using Shop.Models;

namespace Shop.Services
{
    public interface IOrderService
    {
        void OrderProduct(Product product);
        List<Product> GetOrderProducts();
    }
}
