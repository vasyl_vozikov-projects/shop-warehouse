﻿using Shop.Models;

namespace Shop.Services
{
    public interface IProductService
    {
        List<Product> Get();
        Product Get(string id);
    }
}
