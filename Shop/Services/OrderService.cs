﻿using Shop.Models;

namespace Shop.Services
{
    public class OrderService : IOrderService
    {
        private readonly List<Product> _orderProducts = new();
        public List<Product> GetOrderProducts()
        {
            return _orderProducts;
        }
        public void OrderProduct(Product product)
        {
            _orderProducts.Add(product);
        }
    }
}
