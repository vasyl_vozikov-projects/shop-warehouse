﻿using MongoDB.Driver;
using Shop.Models;

namespace Shop.Services
{
    public class ProductService : IProductService
    {
        private readonly IMongoCollection<Product> _products;

        public ProductService(IProductStoreDatabaseSettings settings, IMongoClient mongoClient)
        {
            var database = mongoClient.GetDatabase(settings.DatabaseName);
            _products = database.GetCollection<Product>(settings.ProductsCollectionName);
        }
        public List<Product> Get()
        {
            List<Product> products = _products.Find(product => true).ToList();            
            return products;
        }
        public Product Get(string id)
        {
            return _products.Find(product => product.Id == id).FirstOrDefault();
        }
    }
}