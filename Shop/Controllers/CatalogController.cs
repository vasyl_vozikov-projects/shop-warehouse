﻿using Microsoft.AspNetCore.Mvc;
using Shop.Models;
using Shop.Services;

namespace Shop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly IRabbitMQService _mQService;

        public CatalogController(IProductService productService, IOrderService orderService, IRabbitMQService mQService)
        {
            _productService = productService;
            _orderService = orderService;
            _mQService = mQService;
        }

        [HttpGet]
        public ActionResult<List<Product>> Get()
        {
            return _productService.Get();
        }
        [HttpGet("{id}")]
        public ActionResult<List<Product>> OrderProduct(string id)
        {
            var product = _productService.Get(id);
            var todaysDate = DateTime.Now.ToString("MM/dd/yyyy");

            if (product == null)
            {
                return NotFound($"Product with Id = {id} not found");
            }
            if (product.Quantity < 1)
            {
                _mQService.SendMessage($"Product with Id = {id} is over!");
                return NotFound($"Product with Id = {id} is over!");
            }
            if (product.ExpirationDate == todaysDate)
            {
                _mQService.SendMessage($"Product with Id = {id} is out of expiry!");
                return NotFound($"Product with Id = {id} is out of expiry!");
            }

            _orderService.OrderProduct(product);
            return _orderService.GetOrderProducts();
        }
    }
}
