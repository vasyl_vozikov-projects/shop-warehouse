﻿using Microsoft.AspNetCore.Mvc;
using Warehouse.Models;
using Warehouse.Services;

namespace Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public ActionResult<List<Product>> Get()
        {
            return _productService.Get();
        }

        [HttpGet("{id}")]
        public ActionResult<Product> Get(string id)
        {
            var product = _productService.Get(id);

            if (product == null)
            {
                return NotFound($"Product with Id = {id} not found");
            }
            return product;
        }

        [HttpPost]
        public ActionResult<Product> Post([FromBody] Product product)
        {
            _productService.Create(product);
            return CreatedAtAction(nameof(Get), new {id = product.Id}, product);
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Product product)
        {
            var existingProduct = _productService.Get(id);

            if (existingProduct == null)
            {
                return NotFound($"Product with Id = {id} not found");
            }

            _productService.Update(product, id);
            return Ok($"Product with Id = {id} updated");
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var product = _productService.Get(id);

            if (product == null)
            {
                return NotFound($"Product with Id = {id} not found");
            }

            _productService.Delete(product.Id);
            return Ok($"Product with Id = {id} deleted");
        }
    }
}
