﻿using Warehouse.Models;

namespace Warehouse.Services
{
    public interface IProductService
    {
        List<Product> Get();
        Product Get(string id);
        Product Create(Product product);
        void Update(Product product, string id);
        void Delete(string id);
    }
}
